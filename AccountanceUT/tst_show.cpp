#include <QtTest>
#include <QCoreApplication>

// add necessary includes here

class Show: public QObject
{
    Q_OBJECT

public:
    Show();
    ~Show();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();

};

Show::Show()
{

}

Show::~Show()
{

}

void Show::initTestCase()
{

}

void Show::cleanupTestCase()
{

}

void Show::test_case1()
{

}

QTEST_MAIN(Show)

#include "tst_show.moc"
