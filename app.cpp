#include "app.hpp"
#include "ui_app.h"

#include <QTimer>
#include <QDate>
#include <QPushButton>
#include <QRegularExpression>

#include <QJsonObject>
#include <QJsonDocument>

#include <QFile>

const QStringList App::headerNames = {"Date", "Place", "Article", "Price", "Count", "Total cost"};

App::App(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::App)
{
    // TODO add function to clear button
    ui->setupUi(this);

    ui->dateLineEdit->setValidator(new QRegExpValidator(QRegExp(R"(\d{2}/\d{2}/\d{2})"), this)); // "dd/dd/dd"
    ui->fileName->setValidator(new QRegExpValidator(QRegExp(R"(^(\w*))"), this)); // one_word

    ui->view->setColumnCount(headerNames.size());
    ui->view->setHorizontalHeaderLabels(headerNames);
    ui->view->resizeColumnsToContents();

    ui->saveb->setDisabled(true);

//    ui->dateLineEdit->setText(QDate::currentDate().toString(Qt::ISODate));
    ui->dateLineEdit->setText(QDate::currentDate().toString("dd/MM/yy"));
    ui->date_2->setText(QDate::currentDate().toString("dd_MM_yy"));

    connect((ui->saveb), &QAbstractButton::clicked, this, &App::save);
    connect((ui->loadb), &QAbstractButton::clicked, this, &App::load);
    connect((ui->inputb), &QAbstractButton::clicked, this, &App::insert);
    connect((ui->clearb), &QAbstractButton::clicked, this, &App::clear);

    connect((ui->priceLineEdit), &QLineEdit::textEdited, this, &App::updateTotalCost);
    connect((ui->countLineEdit), &QLineEdit::textEdited, this, &App::updateTotalCost);

    connect((ui->countLineEdit), &QLineEdit::returnPressed, this, &App::insert);
    connect((ui->totalCostLineEdit), &QLineEdit::returnPressed, this, &App::insert);

    connect(this, &App::updateRequest, this, &App::onElementsUpdate);
}

App::~App()
{
    delete ui;
}

///No extra validating - keep @param record_parameters and header names same size
QJsonObject App::insertOneRow(QStringList record_parameters)
{
    QJsonObject record;
    int last_row = ui->view->rowCount();
    ui->view->insertRow(last_row);
    int i = 0;
    for (const auto& field : headerNames)
    {
        const QString& item_text = record_parameters[i];
        record[field] = item_text;
        auto item = new QTableWidgetItem(item_text);
        item->setFlags(Qt::NoItemFlags);
        ui->view->setItem(last_row, i, item);
        i++;
    }
    return record;
}

void App::insert()
{
    QStringList record_parameters{
        ui->dateLineEdit->text(),
        ui->placeLineEdit->text(),
        ui->articleLineEdit->text(),
        ui->priceLineEdit->text(),
        ui->countLineEdit->text(),
        ui->totalCostLineEdit->text(),
    };

    ui->placeLineEdit->setFocus();

    QJsonObject record = insertOneRow(record_parameters);
    qDebug() << record;
    m_records.append(record);  // TODO add day 27/07 from paragons_raw
    ui->saveb->setEnabled(true);

    emit updateRequest();
}

void App::clear()
{
    m_records = QJsonArray();
    emit updateRequest();
}

void App::updateTotalCost()
{
    double value = ui->priceLineEdit->text().replace(",", ".").toDouble();
    double multi = ui->countLineEdit->text().replace(",", ".").toDouble();

    double total = value * multi;

    ui->totalCostLineEdit->setText(QStringLiteral("%1").arg(total, 0, 'f', 2));
}

double App::calculate_total_sum()
{
    double sum = 0;
    int i = 0;
    for (const auto& var : m_records)
    {
        bool is_ok;
        double value = var.toObject()["Total cost"].toString().toDouble(&is_ok);
        if (is_ok)
            sum += value;
        else
        {
            bool is_count_ok;
            qDebug() << "Warning:" << var.toObject()["Total cost"].toString() << "not convertable";
            double count = var.toObject()["Count"].toString().toDouble(&is_count_ok);
            auto price_str = var.toObject()["Price"].toString();
            price_str.replace(',', '.');
            double price = price_str.toDouble(&is_ok);
            double total_price = count * price;
            sum += total_price;
            qDebug() << "Before:" << m_records[i].toObject()["Total cost"].toString() << "Total price =" << total_price;
            QJsonValue tmp_val;
//            tmp_val[]
            auto ref = m_records[i].toObject();
            ref["Total cost"] = QString::number(total_price);
            m_records.replace(i, ref);
            qDebug() << "After:" << m_records[i].toObject()["Total cost"].toString() << "";
        }
        i++;
    }
    return sum;
}

void App::onElementsUpdate()
{
    double sum = calculate_total_sum();
    ui->lab1_2->setText(QStringLiteral("%1").arg(sum, 4, 'f', 2));
    ui->lab2_2->setText(QStringLiteral("%1").arg(m_records.size(), 4));
    updateTableWidget();
}

void App::sort_and_resize()
{
    ui->view->sortItems(0, Qt::DescendingOrder);
    ui->view->resizeColumnsToContents();
}

void App::updateTableWidget()
{
    if (m_records.isEmpty() == true)
    {
        ui->lab1_2->setText(QStringLiteral("%1").arg(0.0, 4, 'f', 2));
        clearTableWidgetView();
    }
    else
        sort_and_resize();
}

void App::clearTableWidgetView()
{
    ui->saveb->setEnabled(false);
    ui->view->clearContents();
    ui->view->setRowCount(0);
}

void App::load()
{
    QString doc_path = "C:\\Users\\Kamil\\Documents\\MyApps\\Accountance\\";
    QString fileName = doc_path + ui->fileName->text() + ".json";
    QFile loadFile(fileName);

    if (!loadFile.open(QIODevice::ReadOnly))
    {
        qWarning("Couldn't open save file.");
        return;
    }

    QByteArray loadData = loadFile.readAll();
    QJsonParseError err;
    QJsonDocument loadDoc(QJsonDocument::fromJson(loadData, &err));
    if (err.error != QJsonParseError::NoError)
    {
        qDebug() << "Load failed!" << err.errorString();
        return;
    }
    if (loadDoc.isArray() == false)
    {
        qDebug() << "Invalid format!";
        return;
    }

    int old_size = m_records.size();
    QJsonArray jarr(loadDoc.array());
    for (const QJsonValueRef& element : jarr)
    {
        m_records.append(element.toObject());
    }
    for (const QJsonValueRef &record : m_records)
    {
        qDebug() << record;
        QStringList record_parameters;
        for (const auto& field : headerNames)
        {
            record_parameters.append(record.toObject()[field].toString());
        }
        insertOneRow(record_parameters);
    }

    updateTableWidget();

    int loaded_entries = m_records.size() - old_size;
    qDebug() << QString("Loaded [%1] entries succesfully!").arg(loaded_entries);
    if (loaded_entries > 0)
    {
        ui->saveb->setEnabled(true);
    }
    // auto_backup("file_name");
//    auto t = new QTimer(this);
//    connect(t, &QTimer::timeout, this, &App::insert);
//    t->start(1000);
    emit updateRequest();
}

void App::save()
{
    if (m_records.isEmpty() == true)
    {
        qDebug() << "Empty records! Aborting";
        return;
    }
    QString doc_path = "C:\\Users\\Kamil\\Documents\\MyApps\\Accountance\\";
    QString fileName = doc_path + ui->fileName->text() + ".json";

    QFile saveFile(fileName);

    if (!saveFile.open(QIODevice::WriteOnly))
    {
        return;
    }

    QJsonDocument saveDoc(m_records);

    if (saveFile.write(saveDoc.toJson()) > 0)
    {
            qDebug() << QString("Saved [%1] entries succefully").arg(m_records.size());
    }
    else
        qDebug() << "Error!";

    saveFile.close();
}

