#ifndef APP_HPP
#define APP_HPP

#include <QWidget>
#include <QJsonArray>

QT_BEGIN_NAMESPACE
namespace Ui { class App; }
QT_END_NAMESPACE

class App : public QWidget
{
    Q_OBJECT

public:
    static const QStringList headerNames;
    enum class Headers {
        date = 0,
        place,
        article,
        price,
        count,
        total_cost
    };
    App(QWidget *parent = nullptr);
    ~App();
signals:
    void updateRequest();
private slots:
    void save();
    void load();
    void insert();
    void clear();

    void updateTotalCost();
    void onElementsUpdate();

private:
    Ui::App *ui;
    QJsonArray m_records;
    QJsonObject insertOneRow(QStringList record_parameters);
    void updateTableWidget();
    void clearTableWidgetView();
    double calculate_total_sum();
    void sort_and_resize();
};
#endif // APP_HPP
